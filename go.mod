module firebase

go 1.15

require (
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/maddevsio/fcm v1.0.5
)
