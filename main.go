package main

import (
	"fmt"
	"log"

	"github.com/maddevsio/fcm"
)

const (
	serverKey = "AAAAHzIa6go:APA91bGHlJDgCDq6fKA6YX2FfMjzl4VQ1vel-JHewj1RL7Q6MSaJ00akdMuMJQPB2XB5ofJ1JD5f_W_DgOs90YjgT4LVLGF6Z4QQAcLeScVzGgetcwQI1zkez0ghd2bmvSB1KJHP1KQp"
)

func mains() {
	data := map[string]string{
		"msg": "Hello World1",
		"sum": "Happy Day",
	}
	c := fcm.NewFCM(serverKey)
	token := "token"
	response, err := c.Send(fcm.Message{
		Data:             data,
		RegistrationIDs:  []string{token},
		ContentAvailable: true,
		Priority:         fcm.PriorityHigh,
		Notification: fcm.Notification{
			Title: "Hello",
			Body:  "World",
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Status Code   :", response.StatusCode)
	fmt.Println("Success       :", response.Success)
	fmt.Println("Fail          :", response.MsgID)
	fmt.Println("Canonical_ids :", response.CanonicalIDs)
	fmt.Println("Topic MsgId   :", response.MsgID)
}
