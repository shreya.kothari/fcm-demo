package main

import (
	"fmt"

	"github.com/NaySoftware/go-fcm"
)

const (
	serverKeys = "AAAAHzIa6go:APA91bHXggN7hijulUvAfaGOF3am82dnQbYDu_ih2oIWvd0e2HAw5d7-D4tpPvvhkPt_CHDMz6czYvq6kPmg1sWw_ok_RPwApIkbQOkdfRqCkfGdQNN559ySlCgVxkvURt2BcaJ97OZ1"
)

func main() {

	var NP fcm.NotificationPayload
	NP.Title = "hello"
	NP.Body = "world"

	data := map[string]string{
		"msg": "Hello World1",
		"sum": "Happy Day",
	}

	ids := []string{
		"token1",
	}

	xds := []string{
		"token5",
		"token6",
		"token7",
	}

	c := fcm.NewFcmClient(serverKeys)
	c.NewFcmRegIdsMsg(ids, data)
	c.AppendDevices(xds)
	c.SetNotificationPayload(&NP)
	status, err := c.Send()
	if err == nil {
		status.PrintResults()
	} else {
		fmt.Println(err)
	}

}
